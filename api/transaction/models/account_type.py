from enum import Enum

class AccountType(Enum):
    CRYPTO = 'Crypto'
    STOCKS = 'Stocks'
    BANK = 'Bank'
    CREDIT = 'Credit'
